function DoGetTelephoneNumber()
{
   if( document.getElementById("tph").checked )
   {
      var elementLI = document.createElement("li");
      var inputfield = document.createElement("input");
      var l = document.createElement("label");
      inputfield.size = "50";
      inputfield.type = "tel";
      inputfield.type = "text";
      inputfield.form = "f1";
      inputfield.pattern = "0[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]";
      inputfield.maxLength = "10";
      inputfield.id = "number";
      inputfield.name= "number";
      inputfield.required = true;
      l.for = "number";
      var elementLU = document.getElementById("list");
      elementLI.appendChild(inputfield);
      elementLI.id = "todelete";
      elementLU.insertBefore( elementLI , elementLU.childNodes[28]);
   }
   else
   {
      document.getElementById("todelete").parentNode.removeChild(document.getElementById("todelete"));
   }
}
function IsNameCorrect(id)
{
   var element = document.getElementById(id);
   var text = document.getElementById(id).value;
   if(text.search(/[0-9]/) != -1)
   {
      element.style.backgroundColor = "rgba(255, 0, 0, 0.267)";
      event.preventDefault();
   }
   else
   {
      element.style.backgroundColor = "white";
   }
}

//graphic




var flag = 0;
function Show( num )
{
  var inflag = 0;
  if( flag == 0)
  {
    var table = document.getElementById("table");
    for( let i=0; i<2; i++)
    {
      var tre = document.createElement("tr");
      if( i == 1 )
      {
        for( let j=0; j<num; j++)
        {
            var tde = document.createElement("td");
            var textfield = document.createElement("input");
            textfield.type = "text";
            textfield.id = i.toString()+j.toString();
            textfield.pattern = "[0-5]";
            tde.appendChild(textfield);
            tre.appendChild(tde);
        }
        table.appendChild(tre);
      }
      else
      {
        for( let j=0; j<num; j++)
        {
            var tde = document.createElement("td");
            var textfield = document.createElement("input");
            textfield.type = "text";
            textfield.id = i.toString()+j.toString();
            textfield.readOnly = true;
            textfield.value = j+1;
            tde.appendChild(textfield);
            tre.appendChild(tde);
        }
        table.appendChild(tre);
      }
    }
    table.hidden = false;
    flag = 1;
  }
  else
  {
      var table = document.getElementById("table");
      table.removeChild(table.lastChild);
      table.removeChild(table.lastChild);
      for( let i=0; i<2; i++)
      {
          var tre = document.createElement("tr");
          if( i == 1 )
          {
            for( let j=0; j<num; j++)
            {
                var tde = document.createElement("td");
                var textfield = document.createElement("input");
                textfield.type = "text";
                textfield.id = i.toString()+j.toString();
                textfield.pattern = "[0-5]";
                tde.appendChild(textfield);
                tre.appendChild(tde);
            }
            table.appendChild(tre);
          }
          else
          {
            for( let j=0; j<num; j++)
            {
                var tde = document.createElement("td");
                var textfield = document.createElement("input");
                textfield.type = "text";
                textfield.id = i.toString()+j.toString();
                textfield.readOnly = true;
                textfield.value = j+1;
                tde.appendChild(textfield);
                tre.appendChild(tde);
            }
            table.appendChild(tre);
          }
      }
      table.hidden = false;
      flaf = 0;
  }
}
function Draw(){
    var num = 0;
    var rbut = document.getElementsByName("Curs");
    var gclass = document.getElementById("myDiv");
    gclass.style.height = 25+"em";
    for( var i=0; i<rbut.length; i++)
    {
      if( rbut[i].checked)
      {
        num = rbut[i].value;
      }
       
    }
    num*=2;
    var x = [];
    var y = [];
    for( let i=0; i<num; i++)
    {
      var v = document.getElementById("0"+i.toString());
      x[i] = v.value;
    }
    for( let i=0; i<num; i++)
    {
      var v = document.getElementById("1"+i.toString());
      y[i] = v.value;
    }
    var trace = {
      x: x,
      y: y,
      type: 'scatter',
      marker: {
        color: 'rgb(161, 35, 73)',
      }
    };
    var data = [trace];
    Plotly.newPlot('myDiv', data);
}
function DrawOnCanvas(){
    var canvas = document.getElementById("mycanvas");
    if (canvas.getContext){
      var ctx = canvas.getContext('2d');

   
    
    ctx.moveTo(14.5,15);
    ctx.lineTo(2,30);
    ctx.lineTo(2,0);
    ctx.fillStyle = "rgb(255, 251, 28)";
    
    ctx.fill();
    }
}