<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<title>My</title>
	<link href="style1.css" rel="stylesheet" type="text/css" />
</head>
                                                                                        
<body onload="Show(2), DrawOnCanvas()">
  <script src="script.js"></script>
  <script src='https://cdn.plot.ly/plotly-latest.min.js'></script>
  <form name="f1"  method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
    <div class="d6"><h1 id="start">Анкета молодого викладача</h1></div>
    <div class="data">
      <ul type="none" id="list" class="border">
        <li class="borderi"><label for="surname">Прізвище</label></li>
        <li><input size="50" maxlength="20" id="surname" name="surname" type="text" onchange="IsNameCorrect('surname')" required><span class="bar"></span></li><br>
        <li class="borderi"><label for="name">Ім'я</label></li>
        <li><input size="50" maxlength="20" id="name" name="name" type="text" required></li><br>   
        
        


        <li class="borderi"><label for="curs">Курс</label></li><br>
        <div class="radio">
          <li><input onclick="Show(2)" name="curs" value="1" type="radio" id="radio1" maxlength="20" checked><label for="radio1">1</label>
            <input onclick="Show(4)" name="curs" value="2" type="radio" id="radio2" maxlength="20"><label for="radio2">2</label>
            <input onclick="Show(6)" name="curs" value="3" type="radio" id="radio3" maxlength="20"><label for="radio3">3</label>
            <input onclick="Show(8)" name="curs" value="4" type="radio" id="radio4" maxlength="20"><label for="radio4">4</label>
          </li><br>
        </div>
        
        <li class="borderi"><label for="faculty">Кафедра</label></li>
        <li>
          <select id="faculty" name="faculty">
            <option value="PZ">Програмне забезпеченя</option>
            <option value="KN">Комп'ютерні науки</option>
            <option value="SA">Системний аналіз</option>
            <option value="Fl">Прикладна лінгвістика</option>
            <option value="VP">Видавництво та поліграфіяч</option>
          </select>
        </li><br>
        <li class="borderi"><label for="email">Елетронна пошта</label></li>
        <li><input size="50" maxlength="30" id="email" name="email" type="email" required></li><br>   
        <br>
        <li class="borderi">Особисті побажання та коментарі</li>
        <li>
          <textarea rows="5" cols="41"></textarea>
        </li>
      </ul>
        <button type="submit" onclick="IsNameCorrect('name'), IsNameCorrect('surname'), Draw()">Надіслати</button>
        <canvas id="mycanvas" height="30px" width="15px"></canvas>
        <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
              $connection = mysqli_connect('localhost:3308', 'root', '', 'laba');
              session_start();
              $name = $_POST['name'];
              $surname = $_POST['surname'];
              $curs = $_POST['curs'];
              $faculty = $_POST['faculty'];
              $email = $_POST['email'];
              $result = mysqli_query($connection, "INSERT INTO `students` (`Name`, `Surname`, `Curs`, `Faculty`, `Email`) VALUES ('$name', '$surname', '$curs', '$faculty', '$email')");
              if($result)
              {
                  header('Location: aboutfaculty.php');
              }
        }
        ?>
      
    </div>
      
    <input type="hidden" name="nulp" value="pupil">
    <div class="main">
      <h3>Середні оцінки за семестровий контроль</h3>
      <table id="table" hidden>
      </table>
    </div>
    <div class="gist" id='myDiv'></div>
    <div class="endblock">
      <h3 id = "end">Наші контакти</h3>
      <a id="linklp" href="https://lpnu.ua/">Національний Університет "Львівська Політехніка"</a>
    </div>
  </form>
</body>
</html>